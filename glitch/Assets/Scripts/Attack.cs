﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Attack
{
	public string name;
	public int damage;
	public bool DOT;

	//Visual feedback
	public AudioClip soundEffect;
	public ParticleSystem VFX;
	public string animationName;

	Vector3 pos;

	public enum attackType
	{
		basic,
		line,
		aoe
	}

	public attackType attack;
}
