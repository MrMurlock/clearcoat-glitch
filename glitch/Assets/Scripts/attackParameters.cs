﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "attack.asset", menuName = "attack/baseAttack")]
public class attackParameters : ScriptableObject
{
	public float damage = 10f;
	public float critMultiplier = 2f;
	public float critChance = 0.3f;
	public float cooldown = 0;
	public float range = 0;
	public AudioClip soundEffect;
	public ParticleSystem VFX;
}
