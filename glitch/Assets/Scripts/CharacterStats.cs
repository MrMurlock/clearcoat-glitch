﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
	public int maxHP;
	[HideInInspector]
	public int currentHP;

	private void Start()
	{
		currentHP = maxHP;
	}

	public void TakeDamage(int damage)
	{
		currentHP -= damage;
		if (currentHP <= 0)
		{
			Debug.Log(name + " is dead.");
			Destroy(gameObject);
		}
	}
}
