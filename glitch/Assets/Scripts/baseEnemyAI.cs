﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class baseEnemyAI : MonoBehaviour
{
	private Controller2D controller;

	public enum EnemyState
	{
		Idle,
		Patrol,
		Chase,
		Attack,
		Death
	}

	public EnemyState currentState;

	public LayerMask whatIsPlayer;

	//patrol variables
	public Transform groundDetection;
	public float patrolSpeed = 5f;
	private bool moveRight = true;

	//chase variables
	public float chaseSpeed = 10f;
	public Transform Target;
	public float chaseRadius = 5f;

	//attack variables
	public float attackRadius = 2f;

	private void Start()
	{
		controller = GetComponent<Controller2D>();
		Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
	}

	private void FixedUpdate()
	{
		switch (currentState)
		{
			case EnemyState.Idle:
				Debug.Log("Idling");
				StartCoroutine("Idle");
				break;
			case EnemyState.Patrol:
				Debug.Log("Patroling");
				StartCoroutine("Patrol");
				break;
			case EnemyState.Chase:
				Debug.Log("Chasing");
				StartCoroutine("Chase");
				break;
			case EnemyState.Attack:
				Debug.Log("Attacking");
				Attack();
				break;
			case EnemyState.Death:
				Debug.Log("Death");
				Death();
				break;
			default:
				break;
		}
	}
	IEnumerator Idle()
	{
		controller.Move(Vector3.down);

		float rand = Random.Range(3f, 10f);

		yield return new WaitForSeconds(rand);

		currentState = EnemyState.Patrol;
		
		yield return null;
	}
	IEnumerator Patrol()
	{
		controller.Move(new Vector3(Time.deltaTime * patrolSpeed, -50 * Time.deltaTime, 0));

		RaycastHit hit;
		Ray ray = new Ray(groundDetection.position, Vector3.down * 0.1f);
		Physics.Raycast(ray, out hit, 0.3f);

		Debug.DrawRay(groundDetection.position, Vector3.down, Color.red);

		if (hit.collider)
		{
			if(moveRight == true)
			{
				transform.eulerAngles = new Vector3(0, -180, 0);
				moveRight = false;
			}
			else
			{
				transform.eulerAngles = new Vector3(0, 0, 0);
				moveRight = true;
			}
			currentState = EnemyState.Idle;
		}
		yield return null;
	}
	IEnumerator Chase()
	{
		controller.Move((Target.position - transform.position).normalized * Time.deltaTime * chaseSpeed + Vector3.down * 50f * Time.deltaTime);
		
		yield return null;
	}
	void Attack()
	{
		return;
	}
	void Death()
	{
		return;
	}

}
