﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour
{
	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;
	public int maxJumps = 2;
	float accelerationTimeAirborn = .2f;
	float accelerationTimeGrounded = .1f;
	private int jumps;

	public int Jumps
	{
		get
		{
			return jumps; 
		}
		set
		{
			jumps = value < 0 ? value : 0;
		}
	}

	public float moveSpeed = 6;
	public float runSpeed = 10;
	public float walkSpeed = 6;


	public float dashSpeed = 20;
	public float dashTime = 1f;
	public float cooldown = 2f;

	float gravity;
	[HideInInspector]
	public Vector3 velocity;
	float jumpVelocity = 8f;
	float velocityXSmoothing;

	Controller2D controller;

	void Start()
	{
		controller = GetComponent<Controller2D>();
		gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		print("Gravity: " + gravity + "Velocity JUMP: " + jumpVelocity);
		jumps = maxJumps;
		jumps--;
	}

    void Update()
	{
		if (controller.collisions.below)
			velocity.y = velocity.y * 0.4f;

		if (Input.GetKey(KeyCode.LeftShift))
			moveSpeed = runSpeed;
		else
			moveSpeed = walkSpeed;
		
		if (controller.collisions.above || controller.collisions.below)
			velocity.y = 0;

		if (controller.collisions.below)
			jumps = maxJumps;
		
		Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		if (Input.GetKeyDown(KeyCode.Space) && (jumps >= 0))
		{
			velocity.y = jumpVelocity;
			jumps--;
		}
		if (Input.GetKeyDown(KeyCode.X))
		{
			StartCoroutine("dash");
		}

		float targetVelocityX = input.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborn);
	
		velocity.y += gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime);
    }
	IEnumerator dash()
	{ 
		moveSpeed = dashSpeed;
		yield return new WaitForSeconds(dashTime);
		moveSpeed = walkSpeed;
		yield return new WaitForSeconds(cooldown);
	}
}
