﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
	public Room startRoomPrefab, endRoomPrefab;
	public List<Room> roomPrefabs = new List<Room>();
	public Vector2 iterationRange = new Vector2(3, 10);

	List<Doorway> availableDoorways = new List<Doorway>();

	StartRoom startRoom;
	EndRoom endRoom;
	List<Room> placedRooms = new List<Room>();

	LayerMask roomLayerMask;

	private void Start()
	{
		roomLayerMask = LayerMask.GetMask("Room");
		StartCoroutine("GenerateLevel");
	}

	IEnumerator GenerateLevel()
	{
		WaitForSeconds startup = new WaitForSeconds(1);
		WaitForFixedUpdate interval = new WaitForFixedUpdate();

		yield return startup;

		//place start room
		PlaceStartRoom();
		yield return interval;

		//random iteration
		int iterations = Random.Range((int)iterationRange.x, (int)iterationRange.y);

		for (int i = 0; i < iterations; i++)
		{
			//place random rooms from list
			PlaceRoom();
			yield return interval;
		}

		//place end room
		PlaceEndRoom();
		yield return interval;

//		yield return new  WaitForSeconds(3);
//		Debug.Log("Reset level generator");
//		ResetLevelGenerator();
	}

	void PlaceStartRoom()
	{
		Debug.Log("placed start room");
		//initialise room
		startRoom = Instantiate(startRoomPrefab) as StartRoom;
		startRoom.transform.parent = this.transform;

		//get doorways
		AddDoorwayToList(startRoom, ref availableDoorways);

		//position room
		startRoom.transform.position = Vector3.zero;
		startRoom.transform.rotation = Quaternion.identity;
	}
	void AddDoorwayToList(Room room, ref List<Doorway> list)
	{
		foreach (Doorway doorway in room.doorways)
		{
			int r = Random.Range(0, list.Count);
			list.Insert(r, doorway);
		}
		//Shuffles the list
		for (int i = 0; i < list.Count; i++)
		{
			Doorway temp = list[i];
			int randomIndex = Random.Range(i, list.Count);
			list[i] = list[randomIndex];
			list[randomIndex] = temp;
		}
	}

	void PlaceRoom()
	{
		Room currentRoom = Instantiate(roomPrefabs[Random.Range(0, roomPrefabs.Count)] as Room);
		currentRoom.transform.parent = this.transform;

		Debug.Log("placed " +currentRoom.name+  " from list");

		List<Doorway> allAvailableDoorways = new List<Doorway>(availableDoorways);
		List<Doorway> currentDoorways = new List<Doorway>();
		AddDoorwayToList(currentRoom, ref currentDoorways);

		AddDoorwayToList(currentRoom, ref availableDoorways);

		bool roomPlaced = false;

		foreach (Doorway availableDoorway in allAvailableDoorways)
		{
			foreach (Doorway currentDoorway in currentDoorways)
			{
				PositionRoomAtDoorway(ref currentRoom, currentDoorway, availableDoorway);

				if (CheckRoomOverlap(currentRoom))
				{
					continue;
				}

				roomPlaced = true;

				placedRooms.Add(currentRoom);

				//Remove occupied doorways
				currentDoorway.gameObject.SetActive(false);
				availableDoorways.Remove(currentDoorway);

				availableDoorway.gameObject.SetActive(false);
				availableDoorways.Remove(availableDoorway);

				//Exit the loop if room has been placed
				break;
			}
			//exit loop if room has been placed
			if (roomPlaced)
			{
				break;
			}

		}

		//room couldnt be placed. Restart
		if(!roomPlaced)
		{
			Destroy(currentRoom.gameObject);
			ResetLevelGenerator();
		}

	}

	void PositionRoomAtDoorway(ref Room room, Doorway roomDoorway, Doorway targetDoorway)
	{
		room.transform.position = Vector3.zero;
		room.transform.rotation = Quaternion.identity;

		//Rotate room to match previous doorway orientation
		Vector3 targetDoorwayEuler = targetDoorway.transform.eulerAngles;
		Vector3 roomDoorwayEuler = roomDoorway.transform.eulerAngles;
		float deltaAngles = Mathf.DeltaAngle(roomDoorwayEuler.y, targetDoorwayEuler.y);
		Quaternion currentRoomTargetRotation = Quaternion.AngleAxis(deltaAngles, Vector3.up);
		room.transform.rotation = currentRoomTargetRotation * Quaternion.Euler(0, 180f, 0);

		//postion room
		Vector3 roomPositionOffset = roomDoorway.transform.position - room.transform.position;
		room.transform.position = targetDoorway.transform.position - roomPositionOffset;
	}

	bool CheckRoomOverlap(Room room)
	{
		Bounds bounds = room.RoomBounds;
		bounds.Expand(-0.1f);

		Collider[] colliders = Physics.OverlapBox(bounds.center, bounds.size / 2, room.transform.rotation, roomLayerMask);
		if (colliders.Length > 0)
		{
			foreach (Collider c in colliders)
			{
				if (c.transform.parent.gameObject.Equals(room.gameObject))
				{
					continue;
				}
				else
				{
					Debug.Log("overlap detected");
					return true;
				}
			}
		}
		return false;
	}

	void PlaceEndRoom()
	{
		Debug.Log("places end room");

		//instaiates room
		endRoom = Instantiate(endRoomPrefab) as EndRoom;
		endRoom.transform.parent = this.transform;

		//create dooraw list to loop over
		List<Doorway> allAvailableDoorways = new List<Doorway>(availableDoorways);
		Doorway doorway = endRoom.doorways[0];

		bool roomPlaced = false;

		foreach (Doorway availableDoorway in allAvailableDoorways)
		{
			//positions the room
			Room room = (Room)endRoom;
			PositionRoomAtDoorway(ref room, doorway, availableDoorway);

			//check if roomoverlaps
			if (CheckRoomOverlap(endRoom))
			{
				continue;
			}

			roomPlaced = true;

			//Remove occupied doorways
			doorway.gameObject.SetActive(false);
			availableDoorways.Remove(doorway);

			availableDoorway.gameObject.SetActive(false);
			availableDoorways.Remove(availableDoorway);

			//Exit the loop if room has been placed
			break;
		}
		//room couldnt be placed. Restart
		if (!roomPlaced)
		{
			Destroy(endRoom.gameObject);
			ResetLevelGenerator();
		}

	}

	void ResetLevelGenerator()
	{
		StopCoroutine(GenerateLevel());

		//delete all rooms
		if (startRoom)
		{
			Destroy(startRoom.gameObject);
		}
		if (endRoom)
		{
			Destroy(endRoom.gameObject);
		}
		foreach (Room room in placedRooms)
		{
			Destroy(room.gameObject);
		}

		placedRooms.Clear();
		availableDoorways.Clear();

		StartCoroutine(GenerateLevel());

	}
}





