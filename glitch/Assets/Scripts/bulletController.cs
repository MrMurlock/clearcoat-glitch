﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletController : MonoBehaviour
{

	public float speed;
	public float airTime;

	private float t;


	private void Start()
	{
		t = airTime;
	}

	private void Update()
	{
		transform.position += Vector3.right * Time.deltaTime * speed;

		if (t <= 0)
		{
			DestroySelf();
		}
		t -= Time.deltaTime;
	}

	private void OnCollisionEnter(Collision c)
	{
		if (c.collider)
		{
			DestroySelf();
		}
	}

	void DestroySelf()
	{
		Destroy(gameObject);
	}

}
