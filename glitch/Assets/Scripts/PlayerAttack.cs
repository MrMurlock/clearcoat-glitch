﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
	public Transform attackPos;
	public LayerMask whatIsEnemy;
	public List<Attack> attacks;

	private void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			switch (attacks[0].attack)
			{
				case Attack.attackType.basic:
					BasicAttack(0);
					break;
				case Attack.attackType.aoe:
					AoeAttack(0);
					break;
				case Attack.attackType.line:
					LineAttack(0);
					break;
			}
		}
		if (Input.GetButtonDown("Fire2") && Input.GetAxisRaw("Vertical") == -1)
		{
			switch (attacks[1].attack)
			{
				case Attack.attackType.basic:
					BasicAttack(1);
					break;
				case Attack.attackType.aoe:
					AoeAttack(1);
					break;
				case Attack.attackType.line:
					LineAttack(1);
					break;
			}

		}
	}

	void BasicAttack(int index)
	{
		Collider[] hit = Physics.OverlapBox(attackPos.position, new Vector2(0.5f, 0.5f), Quaternion.Euler(0,0,0), whatIsEnemy);
		foreach (Collider i in hit)
		{
			if (i != null)
			{
				Debug.Log("You hit with: " + attacks[index].name + " on " + i.name + " and dealt " + attacks[index].damage + " damage!");
				i.GetComponent<CharacterStats>().TakeDamage(attacks[index].damage);
			}
		}
		
		
	}
	void AoeAttack(int index)
	{
		Collider[] hit = Physics.OverlapBox(attackPos.position, new Vector2(0.5f, 0.5f), Quaternion.Euler(0, 0, 0), whatIsEnemy);
		foreach (Collider i in hit)
		{
			if (i != null)
			{
				Debug.Log("You hit with: " + attacks[index].name + " on " + i.name + " and dealt " + attacks[index].damage + " damage!");
				i.GetComponent<CharacterStats>().TakeDamage(attacks[index].damage);
			}
		}
	}
	void LineAttack(int index)
	{

	}
}
