﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
	public Transform shootPos, bullet;

	public int damage = 40;

	private void Update()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			Fire();
		}
	}

	void Fire()
	{
		Instantiate(bullet, shootPos.position, shootPos.rotation);
	}
}
